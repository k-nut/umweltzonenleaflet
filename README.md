# Umweltzonen

A visualization of low emission zones retrieved from OpenStreetMap.


![Umweltzonen-Diff][umweltzonen-diff]

## Usage

To access the website you need to start a local server
in the project directory. Here is an example:

```bash
$ python3 -m http.server
```

This will serve the website on `http://localhost:8000`.


## Dependencies

This frontend depends on a server backend providing
the zone data as a stringified GeoJSON object.
The server backend must serve data via `http://localhost:3000/before`
and `http://localhost:3000/changed`.
Further, `Access-Control-Allow-Origin` must be enabled for the response.




[umweltzonen-diff]: https://bytebucket.org/tbsprs/umweltzonenleaflet/raw/53fafb4a57d26154a55c748605ae0c732264b12a/gfx/umweltzonen-diff.jpg
